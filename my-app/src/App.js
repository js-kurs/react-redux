import React from 'react';
import CommunitySection from './CommunitySection';
import JoinUs from './JoinUs';
import './App.css';

const App = () => {
  return (
    <div className="App">
      <CommunitySection />
      <JoinUs />
    </div>
  );
};

export default App;