import React, { useState } from 'react';

const JoinUs = () => {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(''); //eslint-disable-line
  const [subscribed, setSubscribed] = useState(false);

  const handleInputChange = (event) => {
    setEmail(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setLoading(true);
    try {
      const response = await fetch('http://localhost:3000/subscribe', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email })
      });
      if (!response.ok) {
        const data = await response.json();
        throw new Error(data.error);
      }
      // Handle successful subscription
      setSubscribed(true);
    } catch (error) {
      setError(error.message);
      window.alert(error.message);
    } finally {
      setLoading(false);
    }
  };

  const handleUnsubscribe = async () => {
    setLoading(true);
    console.log('Unsubscribe request:', email);
    try {
      const response = await fetch('http://localhost:3000/unsubscribe', {
        method: 'POST'
      });
      if (!response.ok) {
        const data = await response.json();
        throw new Error(data.error);
      }
      // Handle successful unsubscription
      setSubscribed(false); 
    } catch (error) {
      setError(error.message);
      window.alert(error.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <section className="joinSection">
      <div className='content-conatiner'>
      <h2 className="app-title--join-us">Join Our Program</h2>
      <h3 className="app-subtitle--join-us">Sed do eiusmod tempor incididunt<br />ut labore et dolore magna aliqua.</h3>
      <form onSubmit={handleSubmit}>
        <div className="app-input-container">
          <input
            type="text"
            placeholder="Email"
            className="app-input__field"
            value={email}
            onChange={handleInputChange}
          />
          {subscribed ? (
            <button
              onClick={handleUnsubscribe}
              className="app-section__button app-section__button--unsubscribe"
              disabled={loading}
              style={{ opacity: loading ? 0.5 : 1 }}
            >
              Unsubscribe
            </button>
          ) : (
            <input
              type="submit"
              value={loading ? 'Subscribing...' : 'Subscribe'}
              className="app-section__button app-section__button--submit"
              disabled={loading}
              style={{ opacity: loading ? 0.5 : 1 }}
            />
          )}
        </div>
      </form>
      </div>
    </section>
  );
};

export default JoinUs;
