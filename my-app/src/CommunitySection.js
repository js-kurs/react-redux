import React, { useState, useEffect } from 'react';

const CommunitySection = () => {
  const [communityData, setCommunityData] = useState([]);
  const [sectionVisible, setSectionVisible] = useState(true);

  useEffect(() => {
    const fetchCommunityData = async () => {
      try {
        const response = await fetch('http://localhost:3000/community');
        const data = await response.json();
        setCommunityData(data);
      } catch (error) {
        console.error('Error fetching community data:', error);
      }
    };

    fetchCommunityData();
  }, []);

  const toggleSectionVisibility = () => {
    setSectionVisible(!sectionVisible);
  };

  return (
    <div id="community-conatiner">
      <h1>Big Community of<br></br> People Like You</h1><br></br>
      <div id="btn-container">
        <button onClick={toggleSectionVisibility}>
          {sectionVisible ? 'Hide section' : 'Show section'}
        </button>
      </div>
      <p id="community-text">We’re proud of our products, and we’re really excited<br></br> when we get feedback from our users.</p>
      {sectionVisible && (
        <div id="community">
          {communityData.map(member => (
            <div key={member.id} className="card">
              <img src={member.avatar} alt={`${member.firstName} ${member.lastName}`} />
              <h3>{`${member.firstName} ${member.lastName}`}</h3>
              <p>{member.position}</p>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};


export default CommunitySection;
